package atlas

import (
	"gomud/interp"
	"gomud/player"
	"strings"

	"github.com/orcaman/concurrent-map"
	"github.com/rs/zerolog/log"
)

var PlayerByName cmap.ConcurrentMap
var PlayerByUUID cmap.ConcurrentMap
var PlayerLocation [][][][]map[string]*player.Player

func SetupPlayer() {
	PlayerByName = cmap.New()
	PlayerByUUID = cmap.New()
}

func StartPlayer(p *player.Player) {
	// Set the player up in the atlas and setup the interp
	PlayerByUUID.Set(p.Data.UUID, p)
	p.Interp = interp.NewLogin(p)

	for {
		str, err := p.Input.ReadString('\n')
		if err != nil {
			log.Error().Err(err).Msg("Error reading player input.")
			p.Stop()
			PlayerByUUID.Remove(p.Data.UUID)
			break
		}
		str = strings.TrimSpace(str)
		err = p.Interp.Read(str)
		if err != nil {
			log.Error().Err(err).
				Str("player", p.Data.UUID).
				Msg("Error reading input from player.")
		}
		log.Debug().Msg(str)
	}
}
